FROM python:alpine
# ENV PYTHONUNBUFFERED 1

# CMD mkdir -p /app
WORKDIR /app

RUN apk add --no-cache gcc g++ make libffi-dev python3-dev

COPY . /app
RUN pip install -e /app

CMD ["gunicorn", "library.wsgi:app"]
