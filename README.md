# Library

### Frontend

- Bootstrap 4
- Feather icons

### Backend

- Flask

### Development

```sh
mkvirtualenv -p python3 -a . library
pip install -e .

export FLASK_ENV=development
export FLASK_APP=library
export APP_SETTINGS=config.DevelopmentConfig

flask run -h 0.0.0.0 -p 8000
```

### Production

    git push heroku master
