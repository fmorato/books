from flask_rest_jsonapi import Api, ResourceDetail, ResourceList

from marshmallow_jsonapi.flask import Schema
from marshmallow_jsonapi import fields

from library.models import db, Book


class BookSchema(Schema):
    class Meta:
        type_ = 'book'
        self_view = 'book_detail'
        self_view_many = 'book_list'
        self_view_kwargs = {'id': '<id>'}

    id = fields.Integer(as_string=True, dump_only=True)
    title = fields.Str(requried=True)
    isbn = fields.Str()
    author = fields.Str()


class BookList(ResourceList):
    schema = BookSchema
    data_layer = {'session': db.session,'model': Book}


class BookDetail(ResourceDetail):
    schema = BookSchema
    data_layer = {'session': db.session, 'model': Book}


def init_app(app):
    api = Api(app)
    api.route(BookList, 'book_list', '/api/books')
    api.route(BookDetail, 'book_detail', '/api/books/<int:id>')
    return api

