import os

from flask import Flask


def create_app(config=None):
    """Create and configure an instance of the Flask application."""
    app = Flask(__name__,
                static_folder=os.path.join(os.path.dirname(
                    os.path.abspath(__file__)), "static", "assets"),
                static_url_path="/static",
                instance_relative_config=True)

    app.config.from_object(config or os.environ.get('APP_SETTINGS', 'config.Config'))

    from library.models import db
    db.init_app(app)

    from library.security import security, user_datastore
    security.init_app(app, user_datastore)

    from library import api, library
    api = api.init_app(app)

    app.register_blueprint(library.bp)

    def get_resource_as_string(name, charset='utf-8', folder='assets'):
        with app.open_resource(os.path.join("static", folder, name)) as f:
            return f.read().decode(charset)

    app.jinja_env.globals['get_resource_as_string'] = get_resource_as_string

    @app.before_first_request
    def init_db():
        if app.debug:
            from .data import load_data
            db.create_all()
            load_data()

    return app
