from library.models import db, User, Role, Book
from library.security import user_datastore

users = [{
    'password': 'admin',
    'email': 'admin@example.com'
}, {
    'password': 'user',
    'email': 'user@example.com'
}]
roles = [{'name': 'admin'}, {'name': 'user'}]
role_to_user = [
    ('admin', 'admin@example.com'),
    ('user', 'user@example.com')
]

books = [
    {
        'title': 'The Mists of Avalon',
        'isbn': '0345350499',
                'author': 'Marion Zimmer Bradley'
    }, {
        'title': 'The Firebrand',
        'isbn': '0451459245',
                'author': 'Marion Zimmer Bradley'
    }, {
        'title': 'The Forest House',
        'isbn': '0451454243',
                'author': 'Marion Zimmer Bradley'
    }, {
        'title': 'Programming in C',
        'isbn': '0195687914',
                'author': 'Pradip Dey , Ghosh Manas'
    }, {
        'title': 'Deep Learning with Python',
        'isbn': '1617294438',
                'author': 'Francois Chollet'
    }
]

def load_data():
    """Load sample data to the database"""
    for user in users:
        user_datastore.create_user(password=user['password'], email=user['email'])

    for role in roles:
        user_datastore.create_role(name=role['name'])

    for role, user in role_to_user:
        user_datastore.add_role_to_user(role=role, user=user)

    user_datastore.commit()
    for book in books:
        db.session.add(
            Book(title=book['title'], isbn=book['isbn'], author=book['author']))

    db.session.commit()
