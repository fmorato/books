import os

from flask import Blueprint, flash, redirect, render_template, request, url_for, send_from_directory
from flask_security import login_required

from library.models import db, Book


bp = Blueprint('library', __name__)

from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField
from wtforms.validators import DataRequired


class BookForm(FlaskForm):
    id = IntegerField('id')
    title = StringField('Title', validators=[DataRequired()])
    isbn = StringField('ISBN')
    author = StringField('Authors')


@bp.route('/')
def index():
    """Library home."""
    return render_template('home.html')


@bp.route('/books')
@bp.route('/books/<int:id>')
@bp.route('/books/<string:title>')
@login_required
def books(title=None, id=None):
    """Show all the books."""
    books = []
    q = None
    if title:
        q = Book.query.filter_by(title=title).first()

    elif id:
        q = Book.query.get(id)
        if q:
            return redirect('/books/' + q.title)

    else:
        books = [book for book in Book.query.all()]

    if q:
        books.append(q)

    if (title or id) and not books:
        flash('Book not found', 'warning')
        return redirect('/books')

    form = BookForm()
    page_title = title if title else 'Books'
    return render_template('books.html', title=page_title, books=books, form=form)


@bp.route('/books/remove/<int:id>', methods=["GET"])
@login_required
def remove(id):
    """Remove a book."""
    book = Book.query.get(id)
    if book:
        db.session.delete(book)
        db.session.commit()
        flash('<strong>{}</strong> was removed'.format(book.title), 'success')
    else:
        flash('Book not found', 'warning')
    return redirect(url_for('.books'))


@bp.route('/books/create', methods=["POST"])
@login_required
def create():
    """Create a book."""
    form = BookForm()
    if not form.validate_on_submit():
        print(form.errors)
        flash('There was an error. Try again.', 'warning')
        return redirect(url_for('.books'))

    book = Book(
        title=form.title.data,
        isbn=form.isbn.data,
        author=form.author.data,
    )
    try:
        db.session.add(book)
        db.session.commit()
        flash('<strong>{}</strong> was added'.format(book.title), 'success')
    except:
        db.session.rollback()
        flash('Book not created', 'warning')
    return redirect(url_for('.books'))


@bp.route('/books/update', methods=["POST"])
@login_required
def update():
    """Update a book."""
    form = BookForm()
    if not form.validate_on_submit():
        print(form.errors)
        flash('There was an error. Try again.', 'warning')
        return redirect(url_for('.books'))

    book = Book.query.get(form.id.data)

    book.title = form.title.data
    book.isbn = form.isbn.data
    book.author = form.author.data
    try:
        db.session.commit()
        flash('<strong>{}</strong> was updated'.format(book.title), 'success')
    except:
        db.session.rollback()
        flash('Book not updated', 'warning')
    return redirect(url_for('.books'))


@bp.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(bp.root_path, 'static', 'images'), 'favicon.ico', mimetype='image/vnd.microsoft.icon')
