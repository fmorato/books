import os

from library import create_app
from library.data import load_data
from library.models import db, User

app = create_app("config.ProductionConfig")
with app.app_context():
    db.drop_all()
    db.create_all()
    load_data()

