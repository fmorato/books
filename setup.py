from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()


setup(
    name='library application',
    version='1.0.0',
    description='',
    long_description=readme(),
    keywords='',
    url='https://morato-books.herokkuapp.com',
    author='Felipe Morato',
    author_email='me@fmorato.com',
    license='',
    packages=['library'],
    include_package_data=True,
    install_requires=[
        'bcrypt',
        'flask',
        'flask-rest-jsonapi',
        'flask-security',
        'flask-sqlalchemy',
        'gunicorn',
        'psycopg2-binary',
    ],
    setup_requires=["pytest-runner"],
    tests_require=[
        'pytest',
        'pytest-cov'
    ],
)
