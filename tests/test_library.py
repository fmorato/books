def test_books(client, init_db):
    rv = client.get('/books')
    assert rv.status_code == 200
    assert b'Avalon' in rv.data

    rv = client.get('/books/2', follow_redirects=True)
    assert rv.status_code == 200
    assert b'Firebrand' in rv.data

    rv = client.get('/books/The Firebrand')
    assert rv.status_code == 200
    assert b'Firebrand' in rv.data

    rv = client.get('/books/7', follow_redirects=True)
    assert rv.status_code == 200
    assert b'not found' in rv.data

    rv = client.get('/books/LoL', follow_redirects=True)
    assert rv.status_code == 200
    assert b'not found' in rv.data

def test_remove_book(client, init_db):
    rv = client.get('/books/remove/1', follow_redirects=True)
    assert rv.status_code == 200
    assert b'was removed' in rv.data

    rv = client.get('/books/remove/8', follow_redirects=True)
    assert rv.status_code == 200
    assert b'not found' in rv.data

def test_update_book(client, init_db):
    book = {'id': 1, 'title': 'LoL', 'isbn': 1234, 'author': 'random', 'csrf_token': 'blah'}
    rv = client.post('/books/update', data=book, follow_redirects=True)
    assert rv.status_code == 200
    assert b'was updated' in rv.data
    assert b'LoL' in rv.data
    assert b'Mists' not in rv.data

def test_create_book(client, init_db):
    book = {'title': 'LoL', 'isbn': 1234, 'author': 'random'}
    rv = client.post('/books/create', data=book, follow_redirects=True)
    assert rv.status_code == 200
    assert b'was added' in rv.data
    assert b'LoL' in rv.data

    rv = client.get('/books/6', follow_redirects=True)
    assert rv.status_code == 200
    assert b'LoL' in rv.data

    rv = client.get('/books/LoL')
    assert rv.status_code == 200
    assert b'LoL' in rv.data

