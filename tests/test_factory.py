from library import create_app


def test_config():
    assert not create_app().testing
    assert create_app('config.TestingConfig').testing

def test_index(client):
    rv = client.get('/')
    assert rv.status_code == 200

def test_favicon(client):
    rv = client.get('/favicon.ico')
    assert rv.status_code == 200


