from library.models import Book, User

def test_books(client, init_db):
    b = Book.query.all()
    assert len(b) == 5

    b = Book.query.get(1)
    assert 'Mists' in  b.title

    b = Book(title='LoL', isbn='123', author='Myself')
    init_db.session.add(b)
    init_db.session.commit()

    assert Book.query.filter_by(title='LoL').first()

def test_user(client, init_db):
    assert User.query.filter_by(email='admin@example.com')
