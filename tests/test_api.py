from flask import request, jsonify

from library.models import Book

def test_books(client, init_db):
    with client as c:
        rv = c.get('/api/books')
        assert rv.status_code == 200
        d = rv.get_json()
        assert len(d['data']) == 5

        book = {"data":{"attributes":{"title": "Python Testing","isbn": "5743092837459", "author": "Hard work"}, "type": "book"}}
        rv = c.post('/api/books', json=book)
        assert rv.status_code == 201

def test_book_detail(client, init_db):
    with client as c:
        rv = c.get('/api/books/1')
        assert rv.status_code == 200
        d = rv.get_json()
        assert d['data']["id"] == '1'

        book = {"data":{"attributes":{"title": "LoL"}, "id": "1", "type": "book"}}
        rv = c.patch('/api/books/1', json=book)
        assert rv.status_code == 200
        d = rv.get_json()
        assert d['data']["attributes"]["title"] == "LoL"

        rv = c.delete('/api/books/1')
        assert rv.status_code == 200

