import pytest
from library import create_app
from library.models import db
from library.data import load_data


@pytest.fixture
def app():
    app = create_app('config.TestingConfig')
    yield app


@pytest.fixture
def client(app):
    yield app.test_client()


@pytest.fixture
def init_db(app):
    with app.app_context():
        db.create_all()
        load_data()

        yield db

        db.drop_all()


@pytest.fixture
def runner(app):
    return app.test_cli_runner()
